#include <unistd.h>

/*
	ru -- Become the Root User
	while running the command and arguments given as arguments.
	Use PATH to find command.
*/

/*
 * main --- main program
 */
int main(int argc, char **argv)
{
	if (argc < 2) {
	    write(2, "Usage: ru cmd {arg}\n", 20);
	    return 2;	/* arbitrary nonzero error code; not documented. */
	    }
	(void) setuid((__uid_t) 0);
	(void) setgid((__gid_t) 0);
	if (execvp(argv[1], argv + 1)) {
		perror(argv[1]);
		return 1; }
	/* NOTREACHED */
}
